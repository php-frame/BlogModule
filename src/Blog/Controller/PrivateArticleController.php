<?php
namespace Frame\Module\Blog\Controller;

use Frame\Module\Admin\Controller\ModuleSettingsController;

use Frame\Module\Blog\Model\BlogArticle;
use Frame\Module\Blog\Model\BlogCategory;

class PrivateArticleController extends ModuleSettingsController
{
	protected $file = '@Blog/private/article.twig';

	public function getEdit(){
		return parent::get();
	}

	public function postEdit(){
		//update
		$article = $this->request->getAttribute('current_article');

		foreach($article->getAttributes() as $attribute => $value){
			if($this->param($attribute)){
				$article->$attribute = $this->param($attribute);
			}
		}

		$article->save();

		$this->flash('success', $this->translator->lang('@Blog.article.updated'));

		return $this->redirect('@Blog.settings');
	}

	public function getCreate(){
		return parent::get();
	}

	public function postCreate(){
		// create
		//dd($this->param('category_id'));
		BlogArticle::create([
			'name' => $this->param('name'),
			'slug' => $this->param('slug'),
			'category_id' => $this->param('category_id'),
			'content' => $this->param('content')
		]);

		$this->flash('success', $this->translator->lang('@Blog.article.created'));

		return $this->redirect('@Blog.settings');
	}

	public function getArgs(){
		return [
			'categories' => array_map(function($item){
				return ['value' => $item['id'], 'label' => $item['name']];
			}, BlogCategory::get()->toArray())
		];
	}
}
