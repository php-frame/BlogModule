<?php
namespace Frame\Module\Blog\Controller;

use Frame\Controller\Controller;
use Frame\Module\Admin\Controller\ModuleSettingsController;

use Frame\Module\Blog\Model\BlogArticle;
use Frame\Module\Blog\Model\BlogCategory;

class SettingsController extends ModuleSettingsController
{
    protected $file = '@Blog/pages/settings.twig';

    public function getArgs(){
    	//dd(BlogArticle::get());
    	return [
    		'categories' => BlogCategory::with('article')->get()
    	];
    }
}
