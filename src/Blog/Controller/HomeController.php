<?php
namespace Frame\Module\Blog\Controller;

use Frame\Controller\Controller;

class HomeController extends Controller
{
    public function get(){
    	return $this->render('@Blog/public/article');
    }
}
