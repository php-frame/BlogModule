<?php
namespace Frame\Module\Blog\Controller;

use Frame\Module\Admin\Controller\ModuleSettingsController;

use Frame\Module\Blog\Model\BlogCategory;

class PrivateCategoryController extends ModuleSettingsController
{
	protected $file = '@Blog/private/article.twig';

	public function getEdit(){
		return parent::get();
	}

	public function postEdit(){
		//update
		$category = $this->request->getAttribute('current_category');

		foreach($category->getAttributes() as $attribute => $value){
			if($this->param($attribute)){
				$category->$attribute = $this->param($attribute);
			}
		}

		$category->save();

		return $this->redirect('@Blog.settings');
	}

	public function getCreate(){
		return parent::get();
	}

	public function postCreate(){
		// create
		BlogCategory::create([
			'name' => $this->param('name'),
			'slug' => $this->param('slug')
		]);

		$this->flash('succes', $this->translator->lang('@Blog.category.created'));

		return $this->redirect('@Blog.settings');
	}

	public function getArgs(){
		return [
			'categories' => array_map(function($item){
				return ['value' => $item['id'], 'label' => $item['name']];
			}, BlogCategory::get()->toArray())
		];
	}
}
