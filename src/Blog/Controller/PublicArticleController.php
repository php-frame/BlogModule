<?php
namespace Frame\Module\Blog\Controller;

use Frame\Controller\Controller;

class PublicArticleController extends Controller
{
    public function get(){
    	return $this->render('@Blog/public/article');
    }
}
