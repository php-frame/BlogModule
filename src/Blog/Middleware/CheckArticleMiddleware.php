<?php
namespace Frame\Module\Blog\Middleware;

use Frame\Middleware\Middleware;

use Frame\Module\Blog\Model\BlogArticle;

class CheckArticleMiddleware extends Middleware
{
    public function __invoke($request, $response, $next){
        $routeParams = $request->getAttribute('routeInfo')[2];
    	$article = BlogArticle::where('slug', $routeParams['article_slug'])->first();

    	if($article == null){
    		$this->flash('warning', $this->translator->lang('@Blog.article.404'));

    		return $this->back($request, $response);
    	}

    	$request = $request->withAttribute('current_article', $article);
        $this->view->getEnvironment()->addGlobal('current_article', $article);

    	$response = $next($request, $response);
        return $response;
    }
}
