<?php
namespace Frame\Module\Blog\Middleware;

use Frame\Middleware\Middleware;

use Frame\Module\Blog\Model\BlogCategory;

class CheckCategoryMiddleware extends Middleware
{
    public function __invoke($request, $response, $next){
    	$routeParams = $request->getAttribute('routeInfo')[2];
        $category = BlogCategory::where('slug', $routeParams['category_slug'])->first();

    	if($category == null){
    		$this->flash('warning', $this->translator->lang('@Blog.category.404'));

    		return $this->back($request, $response);
    	}

    	$request = $request->withAttribute('category', $category);
        $this->view->getEnvironment()->addGlobal('current_category', $category);

    	$response = $next($request, $response);
        return $response;
    }
}
