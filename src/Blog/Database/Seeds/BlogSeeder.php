<?php


use Phinx\Seed\AbstractSeed;

class BlogSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $articles = [];

        for($i = 0 ; $i < 100 ; $i++){
            $articles[] = [
                'name' =>
            ];
        }

        $this->table('blog_articles')
            ->insert($articles)
            ->save();
    }
}
