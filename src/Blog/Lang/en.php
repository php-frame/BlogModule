<?php

return [
    'article' => [
        'create' => 'Create an article',
        'edit' => 'Edit an article',
        'created' => 'Article created',
        'updated' => 'Article updated'
    ],
    'category' => [
    	'created' => 'Category created',
    	'updated' => 'Category updated'
    ]
];