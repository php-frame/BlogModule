<?php
namespace Frame\Module\Blog\Model;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $fillable = [ 'name', 'slug' ];

    public function article(){
    	return $this->hasMany(BlogArticle::class, 'category_id');
    }
}