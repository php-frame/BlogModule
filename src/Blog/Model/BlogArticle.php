<?php
namespace Frame\Module\Blog\Model;

use Illuminate\Database\Eloquent\Model;

class BlogArticle extends Model
{
    protected $fillable = [ 'name', 'slug', 'content', 'category_id' ];

    public function category(){
    	return $this->belongsTo(BlogCategory::class);
    }
}