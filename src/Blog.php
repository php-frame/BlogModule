<?php
namespace Frame\Module;

use Aptoma\Twig\Extension\MarkdownExtension;
use Aptoma\Twig\Extension\MarkdownEngine;

// Controllers
use Frame\Module\Blog\Controller\SettingsController;
use Frame\Module\Blog\Controller\PublicCategoryController;
use Frame\Module\Blog\Controller\PublicArticleController;
use Frame\Module\Blog\Controller\PrivateCategoryController;
use Frame\Module\Blog\Controller\PrivateArticleController;

// Middleware 
use Frame\Module\Admin\Middleware\AdminMiddleware;
use Frame\Module\Blog\Middleware\CheckCategoryMiddleware;
use Frame\Module\Blog\Middleware\CheckArticleMiddleware;

use Frame\Module\Blog\Model\BlogArticle;

class Blog extends \Frame\Module
{
	const MIGRATIONS = __DIR__ . '/Blog/Database/Migrations';
	const VIEWS = __DIR__ . '/Blog/Ressources/views';

    public function __invoke(){
		$engine = new MarkdownEngine\ParsedownEngine();

    	$that = $this;
    	$c = $this->app->getContainer();
		
		$c->view->addExtension(new MarkdownExtension($engine));

		$this->app->group('/admin/blog', function() use ($c, $that){
			$this->map(['GET'], '', SettingsController::class)->setName($that->routeName('admin.settings'));

			/*$this->group('/article', function() use ($that){
				$this->route(['GET', 'POST'], '/create', [PrivateArticleController::class, 'create'])->setName($that->routeName('private.article.create'));
				$this->route(['GET', 'POST'], '/{article_slug}/edit', [PrivateArticleController::class, 'edit'])->setName($that->routeName('private.article.edit'))->add(CheckArticleMiddleware::class);
				$this->route(['GET'], '/{article_slug}/delete', [PrivateArticleController::class, 'delete'])->setName($that->routeName('private.article.delete'))->add(CheckArticleMiddleware::class);
			});*/
			$this->group('/category', function() use ($that){
				$this->map(['POST'], '/create', [PrivateCategoryController::class, 'create'])->setName($that->routeName('private.category.create'));

				$this->group('/{category_slug}', function() use ($that){
					$this->map(['GET', 'POST'], '/edit', [PrivateCategoryController::class, 'edit'])->setName($that->routeName('private.category.edit'));
					$this->map(['GET'], '/delete', [PrivateCategoryController::class, 'delete'])->setName($that->routeName('private.category.delete'));

					$this->map(['GET', 'POST'], '/create', [PrivateArticleController::class, 'create'])->setName($that->routeName('private.article.create'));
					$this->map(['GET', 'POST'], '/{article_slug}/edit', [PrivateArticleController::class, 'edit'])->setName($that->routeName('private.article.edit'))->add(CheckArticleMiddleware::class);
					$this->map(['GET'], '/{article_slug}/delete', [PrivateArticleController::class, 'delete'])->setName($that->routeName('private.article.delete'))->add(CheckArticleMiddleware::class);
				})->add(CheckCategoryMiddleware::class);
			});
		})->add(new AdminMiddleware($c));

		$this->app->group('/content', function() use ($c, $that){
			$this->group('/{category_slug}', function() use ($c, $that){
				$this->map(['GET'], '', PublicCategoryController::class)->setName($that->routeName('public.category'));
				$this->map(['GET'], '/{article_slug}', PublicArticleController::class)->setName($that->routeName('public.article'))->add(CheckArticleMiddleware::class);
			})->add(CheckCategoryMiddleware::class);
		});
    }
}