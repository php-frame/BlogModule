<?php

require __DIR__ . '/vendor/autoload.php';
session_start();

$app = new \Frame\Core([
	\Frame\Module\Auth::class,
	\Frame\Module\Admin::class,
	\Frame\Module\Blog::class
]);

return \Frame\PhinxConfig($app);
